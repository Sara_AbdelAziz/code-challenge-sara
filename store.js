const fs = require('fs');

//1-add mykey myvalue
module.exports.add=function(mykey,myvalue){
  fs.readFile('data.json', 'utf8', (err, data) =>{
      if (err) throw err;
      let obj = JSON.parse(data);
      obj[mykey]=myvalue;
      fs.writeFile('data.json', JSON.stringify(obj, null, 2), 'utf8', (err) =>{
        if (err) throw err;
      });
  });
}

//2-list
module.exports.list =fs.readFile('data.json', 'utf8',(err, data) => {
      if (err) throw err;
      console.log(JSON.parse(data));
});

//3-get mykey
module.exports.get=function(myKey){
  fs.readFile('data.json',  (err, data) =>{

      let obj = JSON.parse(data);
      for(var i in obj){
        if (myKey == i){
           console.log(`The value of ${myKey} is ${obj[i]}`);
        }
      }
  });
}

//4-remove mykey
module.exports.remove=function(myKey){
  fs.readFile('data.json', 'utf8', (err, data) =>{
      if (err) throw err;
      let obj = JSON.parse(data);
      delete obj[myKey];
      fs.writeFile('data.json', JSON.stringify(obj, null, 2), 'utf8', (err) =>{
        if (err) throw err;
      });
  });
}

//5-clear
module.exports.clear=function(){
fs.writeFile('data.json','' ,() =>{console.log('all data has been removed')})
}

require('make-runnable');
